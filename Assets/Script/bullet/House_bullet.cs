using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class House_bullet : MonoBehaviour
{
    // Start is called before the first frame update
    private List<Collider2D> col = new List<Collider2D>();
    public int my_damage = 0;
    private float time = 0;
    public float delay = 2;
    private float startdelay = 2;
    private SpriteRenderer sRender;
    void Start()
    {
        sRender = GetComponent<SpriteRenderer>();
        startdelay = delay;

    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        sRender.color = new Color(1, 1, 1, time / 3);
        if(time >= 3)
        {
            delay -= Time.deltaTime;
        }
        if(time >= 10)
        {
            Destroy(gameObject);
        }
        
        foreach(Collider2D minicol in col)
        {
            if (minicol.gameObject.tag == "Enemy" && time >= 3 && delay <= 0)
            {
                minicol.gameObject.GetComponent<Character>().health -= my_damage;
                var R2d = minicol.gameObject.GetComponent<Rigidbody2D>();
                R2d.velocity = new Vector2(2, 0);
                
            }
        }

        if (delay <= 0)
        {
            delay = startdelay;
        }

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy" && time >= 3)
        {
            collision.gameObject.GetComponent<Character>().health -= my_damage;
            var R2d = collision.gameObject.GetComponent<Rigidbody2D>();
            R2d.velocity = new Vector2(2, 0);
        }
        col.Add(collision);
    }


    public void OnTriggerExit2D(Collider2D collision)
    {
        col.Remove(collision);
    }
}

