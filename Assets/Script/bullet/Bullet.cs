using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Start is called before the first frame update
    private Collider2D col;
    private bool IsAlly = false;
    public float speed = 0.005f;
    public int my_damage = 0;
    void Start()
    {
        col = GetComponent<Collider2D>();
        if(gameObject.tag == "Ally_bullet")
        {
            IsAlly = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (IsAlly)
        {
            this.transform.position = new Vector2(this.transform.position.x + speed, this.transform.position.y);
        }
        else
        {
            this.transform.position = new Vector2(this.transform.position.x - speed, this.transform.position.y);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (IsAlly)
        {
            if (collision.gameObject.tag == "Enemy")
            {
                collision.gameObject.GetComponent<Character>().health -= my_damage;
                Destroy(gameObject);
            }
        }
        else
        {
            if (collision.gameObject.tag == "Ally")
            {
                collision.gameObject.GetComponent<Character>().health -= my_damage;
                Destroy(gameObject);
            }
        }
    }
    

}
