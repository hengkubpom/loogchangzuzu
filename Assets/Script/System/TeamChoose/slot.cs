using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class slot : MonoBehaviour
{
    // Start is called before the first frame update
    static public int[] slotnumberchar = new int[7];
    private Image this_slot_image;
    public int this_slot_number = 1;
    void Start()
    {
        this_slot_image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        if (slotnumberchar[this_slot_number] == 0)
        {
            this_slot_image.sprite = TeamChoose.profile_character_s[0];
        }
    }

    public void SelectSlot()
    {
        for(int i=0;i<slotnumberchar.Length;i++)
        {
            if(TeamChoose.character_num == slotnumberchar[i])
            {
                slotnumberchar[i] = 0;
                break;
            }
        }
        slotnumberchar[this_slot_number] = TeamChoose.character_num;
        this_slot_image.sprite = TeamChoose.profile_character_s[TeamChoose.character_num];
    }
}
