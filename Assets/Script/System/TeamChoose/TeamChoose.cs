using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TeamChoose : MonoBehaviour
{
    // Start is called before the first frame update
    static public int character_num = 0;
    public GameObject slotpanel;
    public Sprite[] profile_character;
    public GameObject[] character_ally;
    static public Sprite[] profile_character_s;
    static public GameObject[] character_ally_s;
    void Start()
    {
        profile_character_s = profile_character;
        character_ally_s = character_ally;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenSlot(int character_number)
    {
        character_num = character_number;
        slotpanel.SetActive(true);
    }
    public void CloseSlot()
    {
        character_num = 0;
        slotpanel.SetActive(false);
    }
    

}
