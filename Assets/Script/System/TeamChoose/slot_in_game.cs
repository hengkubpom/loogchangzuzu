using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class slot_in_game : MonoBehaviour
{
    private Image this_slot_image;
    public int this_slot_number = 1;
    public slot_countdown countdown_slot;
    private float slot_cooldown = 10;
    private float slot_use_mana = 0;
    private float onCooldown = 0;
    // Start is called before the first frame update
    void Start()
    {
        this_slot_image = GetComponent<Image>();
        slot_cooldown = TeamChoose.character_ally_s[slot.slotnumberchar[this_slot_number]].gameObject.GetComponent<Character>().cooldown;
        slot_use_mana = TeamChoose.character_ally_s[slot.slotnumberchar[this_slot_number]].gameObject.GetComponent<Character>().use_mana;
    }

    // Update is called once per frame
    void Update()
    {
        if (slot.slotnumberchar[this_slot_number] != 0)
        {
            this_slot_image.sprite = TeamChoose.profile_character_s[slot.slotnumberchar[this_slot_number]];
        }
        else
        {
            this_slot_image.sprite = TeamChoose.profile_character_s[0];
        }

        if(onCooldown > 0)
        {
            onCooldown -= Time.deltaTime;
        }
    }

    public void summon()
    {
        if(onCooldown <= 0 && mana._mana > slot_use_mana)
        {
            if (slot.slotnumberchar[this_slot_number] != 0)
            {
                mana._mana -= slot_use_mana;
                var Ranger = Instantiate(TeamChoose.character_ally_s[slot.slotnumberchar[this_slot_number]]);

                Ranger.transform.position = Info_value.Base_ally_position;
                onCooldown = slot_cooldown;
                countdown_slot.gameObject.SetActive(true);
                countdown_slot.Cooldown = slot_cooldown;
            }
        }
    }
}
