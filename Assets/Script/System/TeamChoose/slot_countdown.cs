using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class slot_countdown : MonoBehaviour
{
    // Start is called before the first frame update
    public Text _text;
    public float Cooldown;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        _text.text = Convert.ToString((int)Cooldown);
        Cooldown -= Time.deltaTime;
        if (Cooldown <= 0)
        {
            gameObject.SetActive(false);
        }
    }
}
