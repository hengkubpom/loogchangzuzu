using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mana : MonoBehaviour
{
    // Start is called before the first frame update
    static public float _mana = 0;
    static public float max_mana = 100;
    public float mana_speed = 0.01f;
    private int level_mana = 1;
    private int need_mana = 0;
    private Text _text;
    void Start()
    {
        _text = GetComponent<Text>();
        need_mana = (int)(max_mana * 0.75f);
    }

    // Update is called once per frame
    void Update()
    {
        
        _text.text = (int)_mana + "/" + max_mana;
        if (_mana < max_mana)
        {
            _mana += mana_speed;
        }
        Debug.Log(need_mana);
    }

    public void Upgrade_mana()
    {
        if(level_mana <= 5 && _mana >= need_mana)
        {
            _mana -= need_mana;
            level_mana += 1;
            max_mana += 200;
            mana_speed += 0.01f;
            need_mana = (int)(max_mana / 1.75f);
        }
    }
}
