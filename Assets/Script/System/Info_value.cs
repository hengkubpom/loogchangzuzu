using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Info_value : MonoBehaviour
{
    static public int stage = 0;
    static public int allow_stage = 1;
    static public Vector3 Base_ally_position = Vector3.zero;
    static public Vector3 Base_enemy_position = Vector3.zero;
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);
        
    }

    // Update is called once per frame
    void Update()
    {

        switch (stage)
        {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                Base_ally_position = new Vector3(-9.5f, -1, 0);
                Base_enemy_position = new Vector3(9.5f, -1, 0);
                break;
            default:
                Base_ally_position = new Vector3(-20f, -1, 0);
                Base_enemy_position = new Vector3(20f, -1, 0);
                break;
        }

        
    }
}
