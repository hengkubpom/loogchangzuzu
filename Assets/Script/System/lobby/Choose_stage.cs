using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Choose_stage : MonoBehaviour
{
    // Start is called before the first frame update
    public Rigidbody2D[] stage_button;
    private SpriteRenderer[] button;
    private Vector3 mousePos;
    private Camera cam;
    private bool selected = false;
    void Start()
    {
        button = new SpriteRenderer[stage_button.Length];
        cam = GetComponent<Camera>();
        for(int i=0;i<stage_button.Length; i++)
        {
            button[i] = stage_button[i].GetComponent<SpriteRenderer>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = -2;
        for (int i = 0;i<stage_button.Length;i++)
        {
            if (stage_button[i].OverlapPoint(mousePos) && Input.GetMouseButtonUp(0))
            {
                Info_value.stage = i+1;
                for(int j = 0; j < stage_button.Length; j++)
                {
                    button[j].color = Color.green;
                }
                button[i].color = Color.red;
                selected = true;
            }
        }
    }

    public void StartGame()
    {
        if (selected)
        {
            SceneManager.LoadScene("TeamChoose");
        }
    }
}
