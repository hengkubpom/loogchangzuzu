using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEditor.SceneManagement;

public class CameraWalk : MonoBehaviour
{
    // Start is called before the first frame update
    private Camera _camera;
    private CameraWalk cam_walk;
    private Vector3 first_position = Vector3.zero;
    private Vector3 mousePos, oldPos, distancetomove;
    private bool IsClick = false;
    private float zoom;
    public bool h = true;
    public float limit_max_zoom = 1000;
    public float limit_min_zoom = 5;
    void Start()
    {
        _camera = GetComponent<Camera>();
        cam_walk = GetComponent<CameraWalk>();
        if (SceneManager.GetActiveScene().name == "stage")
        {
            switch (Info_value.stage)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    cam_walk.limit_max_zoom = 6;
                    break;
                default:
                    cam_walk.limit_max_zoom = 12;
                    break;
            }
        }
        _camera.orthographicSize = limit_max_zoom;
    }

    // Update is called once per frame
    void Update()
    {
        mousePos = Input.mousePosition;
        if (Input.GetMouseButtonDown(0))
        {
            IsClick = true;
            
        }
        else if (Input.GetMouseButtonUp(0))
        {
            IsClick = false;
        }



        if (mousePos != oldPos && IsClick)
        {
            this.transform.position += new Vector3(-1*((mousePos.x - oldPos.x)/30), 0, 0);
            if (h)
            {
                this.transform.position += new Vector3(0, -1 * ((mousePos.y - oldPos.y) / 30), 0);
            }
        }
        if(this.transform.position.x < -((limit_max_zoom - _camera.orthographicSize)*1.8f))
        {
            this.transform.position = new Vector3(-((limit_max_zoom - _camera.orthographicSize) * 1.8f), this.transform.position.y, -10);
        }
        else if (this.transform.position.x > (limit_max_zoom - _camera.orthographicSize)*1.8f)
        {
            this.transform.position = new Vector3((limit_max_zoom - _camera.orthographicSize) * 1.8f, this.transform.position.y, -10);
        }

        if (this.transform.position.y < -(limit_max_zoom - _camera.orthographicSize))
        {
            this.transform.position = new Vector3(this.transform.position.x, -(limit_max_zoom - _camera.orthographicSize), -10);
        }
        else if (this.transform.position.y > limit_max_zoom - _camera.orthographicSize)
        {
            this.transform.position = new Vector3(this.transform.position.x, limit_max_zoom - _camera.orthographicSize, -10);
        }

        //this.transform.position += new Vector3(0, 0, Input.mouseScrollDelta.y / 3);
        _camera.orthographicSize -= (Input.mouseScrollDelta.y / 3);
        if(_camera.orthographicSize > limit_max_zoom)
        {
            _camera.orthographicSize = limit_max_zoom;
        }
        else if(_camera.orthographicSize < limit_min_zoom)
        {
            _camera.orthographicSize = limit_min_zoom;
        }

        oldPos = mousePos;
    }
}
