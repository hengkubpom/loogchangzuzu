using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base : MonoBehaviour
{
    // Start is called before the first frame update
    static public int max_health = 20;
    public float Cooldown_laser = 30;
    private float cooldown = 0;
    private bool onCooldown = false;
    private bool IsAlly = false;
    private Character self;
    public House_bullet bullet;
    
    void Start()
    {

        self = GetComponent<Character>();
        self.health = max_health;
        if (gameObject.tag == "Ally")
        {
            IsAlly = true;
            this.transform.position = Info_value.Base_ally_position;
        }
        else
        {
            IsAlly = false;
            this.transform.position = Info_value.Base_enemy_position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(self.health <= 0)
        {
            //end game
            if (IsAlly)
            {
                //lose
                Debug.Log("lose");
            }
            else
            {
                //win
                Debug.Log("win");
            }
            
        }
        if (onCooldown)
        {
            cooldown += Time.deltaTime;
            if(cooldown >= Cooldown_laser)
            {
                onCooldown = false;
            }
        }

    }

    public void Shoot(slot_countdown countdown_item)
    {
        if (!onCooldown)
        {
            onCooldown = true;
            var sbullet = Instantiate(bullet);
            sbullet.transform.position = new Vector3(Info_value.Base_ally_position.x, Info_value.Base_ally_position.y - 1, Info_value.Base_ally_position.z+1);
            countdown_item.gameObject.SetActive(true);
            countdown_item.Cooldown = Cooldown_laser;
        }
    }
}
