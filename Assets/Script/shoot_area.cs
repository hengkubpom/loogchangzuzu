using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shoot_area : MonoBehaviour
{
    // Start is called before the first frame update
    public Character myCharacter;
    private bool IsAlly = false;
    void Start()
    {
        if(myCharacter.gameObject.tag == "Ally")
        {
            IsAlly = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (IsAlly)
        {
            if (collision.gameObject.tag == "Enemy" && !myCharacter.OnFight && collision != null)
            {
                myCharacter.enemy_enter_area(collision.gameObject.GetComponent<Character>());
            }
        }
        else
        {
            if (collision.gameObject.tag == "Ally" && !myCharacter.OnFight && collision != null)
            {
                myCharacter.enemy_enter_area(collision.gameObject.GetComponent<Character>());
            }
        }

    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (IsAlly)
        {
            if (collision.gameObject.tag == "Enemy" && !myCharacter.OnFight && collision != null)
            {
                myCharacter.enemy_enter_area(collision.gameObject.GetComponent<Character>());
            }
        }
        else
        {
            if (collision.gameObject.tag == "Ally" && !myCharacter.OnFight && collision != null)
            {
                myCharacter.enemy_enter_area(collision.gameObject.GetComponent<Character>());
            }
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        myCharacter.OnFight = false;
    }

}
