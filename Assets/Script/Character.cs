using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    // Start is called before the first frame update
    private bool IsAlly = false;
    private bool allow_knockback = true;
    private Character enemy;
    private float timestand = 0;
    private SpriteRenderer sprite;
    private int max_health = 0;
    private Rigidbody2D rb;
    private Animator animator;
    public bool OnFight = false;
    public float speed = 0.001f;
    public bool melee = true;
    public Bullet bullet;
    public float bullet_speed = 0.05f;
    public int health = 20;
    public int do_damage = 5;
    public int delay_damage = 3;
    public bool IsBase = false;
    public int cooldown = 0;
    public int use_mana = 0;
    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        max_health = health;
        sprite = GetComponent<SpriteRenderer>();
        timestand = delay_damage;
        OnFight = false;
        if (gameObject.tag == "Ally")
        {
            IsAlly = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(OnFight);
        if (enemy == this || enemy == null)
        {
            OnFight = false;
        }
        if (health <= 0 && !IsBase)
        {
            Destroy(gameObject);
        }
        else if(health <= max_health / 2 && allow_knockback)
        {
            if (IsAlly)
            {
                rb.velocity = new Vector2(-2, 0);
                allow_knockback = false;
            }
            else
            {
                rb.velocity = new Vector2(2, 0);
                allow_knockback = false;
            }
        }

        if(rb.velocity.x > 0)
        {
            rb.velocity = new Vector2(rb.velocity.x - 0.01f, 0);
        }
        else if(rb.velocity.x < 0)
        {
            rb.velocity = new Vector2(rb.velocity.x + 0.01f, 0);
        }

        if (!OnFight)
        {
            timestand += Time.deltaTime;
            if (timestand >= delay_damage)
            {
                timestand = delay_damage;
                if (!IsBase)
                {
                    //animator.Play("Walk");
                }
                if (IsAlly)
                {
                    this.transform.position = new Vector2(this.transform.position.x + speed, this.transform.position.y);
                }
                else
                {
                    this.transform.position = new Vector2(this.transform.position.x - speed, this.transform.position.y);
                }
            }
            
        }
        else
        {
            timestand += Time.deltaTime;
            if (timestand >= delay_damage)
            {
                timestand = 0;
                if(!IsBase)
                {
                    //animator.Play("Attack");
                }
                if (melee)
                {
                    go_damage();
                }
                else
                {
                    range_damage();
                }
            }
                
        }

    }

    public void enemy_enter_area(Character enemy)
    {
        this.enemy = enemy;
        OnFight = true;
    }

    private void go_damage()
    {
        enemy.health -= do_damage;
    }

    private void range_damage()
    {
        var mybullet = Instantiate(bullet);
        mybullet.transform.position = this.transform.position;
        mybullet.my_damage = do_damage;

    }

    
}
